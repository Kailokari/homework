﻿using BowlingWithFrame;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace BowlingTest

{

    [TestFixture]
    public class Keilaustesti
    {
        Keilaluokka heitto;

        [SetUp]
        public void TestienAlustus()
        {
            heitto = new Keilaluokka();
        }

        [Test]
        public void TestiToimii()
        {
            Assert.IsTrue(true);
        }

        private void Heitot(int roll1, int roll2, int numberOfFrames)
        {
            for (int frame = 0; frame < numberOfFrames; frame++)
            {
                heitto.RollFrame(roll1, roll2);
            }
        }




        [Test]
    public void RanniPalloAina0Pistetta()
    {

            /*for (int frame = 1; frame <= 10; frame++)
            {
                heitto.RollFrame(0, 0);
            }
            */
            Heitot(0, 0, 10);
        Assert.AreEqual(0, heitto.Score, "Ränni pallo antaa aina 0 pistettä");
    }

       


        [Test]
        public void Aina2HeittoaJa40Pistetta()
        {
           
            for (int frame = 1; frame <= 10; frame++)                       //korjaa tiiviimmäksi?
            {
                heitto.RollFrame(2, 2);
            }
            Assert.AreEqual(40, heitto.Score, "Aina 2 heittoa saa yhteensä 40 pistettä");
        }


        [Test]
        public void EnsimmainenHeittoPaikkausJa2SeuraavaaHeittoa48Pistetta()
        {
            heitto.RollSpare(2, 8);
            Heitot(2, 2, 9);
            Assert.AreEqual(48, heitto.Score, "3 heittoa, ensimmäinen paikko, yhteensä yhteensä 48 pistettä");
        }

        

    }


}