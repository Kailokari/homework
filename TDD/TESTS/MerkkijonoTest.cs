﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDD;

namespace TESTS
{
    public class MerkkijonoTest
    {
        Merkkijonolaskin laskin;            // Merkkijonolaskin laskin = new Merkkijonolaskin();

        [SetUp]
        public void TestienAlustus()
        {
            laskin = new Merkkijonolaskin(); 
        }


        [Test]
        public void TyhjaMerkkijonoPalauttaaNollan()
        {
       // Merkkijonolaskin laskin = new Merkkijonolaskin();

        Assert.That(laskin.Summa(""), Is.EqualTo(0));
            }

        [Test]
        public void YksiAnnettuLukuPalauttaaLuvunArvon()
        {
         //   Merkkijonolaskin laskin = new Merkkijonolaskin();

            Assert.That(laskin.Summa("5"), Is.EqualTo(5)); 
        }

        [Test]
        public void KaksiLukuaPilkullaErotettunaPalauttaaNollan()
        {
           // Merkkijonolaskin laskin = new Merkkijonolaskin();

            Assert.That(laskin.Summa("1,2"), Is.EqualTo(3));

        }

        [Test]
        public void NumeroitaAnnetaanUseampiKuinKaksi()
        {
            Assert.That(laskin.Summa("1,2,3,4,5"), Is.EqualTo(15));
        }

    }
}
