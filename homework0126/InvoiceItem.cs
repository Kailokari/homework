﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework2601
{
    class InvoiceItem
    {
        string id;
        string desc;
        int qty;
        double unitPrice;

        string getId()
        {
            return id;
        }
        
        string getDesc()
        {
            return desc;
        }

        int getQty()
        {
            return qty;
        }

        void setQty(int qty)
        {
            this.qty = qty;
        }

        double getUnitPrice()
        {
            return unitPrice;
        }

        void setUnitPrice(double unitPrice)
        {
            this.unitPrice = unitPrice;
        }

        public override String ToString()

        {

            return id + desc + qty + "  " + unitPrice+"   ";
        }
    }
}
