﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework2601
{
    public class Date
    {
        int day;
        int month;
        int year;

        int getDay()
        {
            return day;
        }

        void setDay(int day)
        {
            this.day = day;
        }

        int getMonth()
        {
            return month;
        }

        void setMonth(int month)
        {
            this.month = month;
        }

        int getYear()
        {
            return year;
        }

        void setYear(int year)
        {
            this.year = year; 
        }

        public override String ToString() => year + "    " + month + "  " + day + "  ";

    }


}
