﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework2601
{
    public class Employee
    {

        int id;
        int salary;
        string firstName;
        string lastName;

        int getId()
        {
            return id;
        }

        string getFirstName()
        {
            return firstName;
        }

        string getLastName()
        {
            return lastName;
        }

        int getSalary()
        {
            return salary;
        }

        void setSalary(int salary)
        {
            this.salary = salary;
        }


        public override String ToString()

        {

            return id + firstName + lastName + salary+" ";
        }

       
    }
}
